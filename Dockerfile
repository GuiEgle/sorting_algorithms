FROM alpine

RUN apk update && apk add g++ make cmake valgrind bash

WORKDIR /sorting_algorithms

COPY . .

RUN cmake -S . -Bbuild

RUN cd build && make

RUN mkdir -p results

CMD chmod +x run_tests.sh && ./run_tests.sh
