from plotly import graph_objects as go
from pandas import read_csv

bubble = read_csv("bubble.csv")
insertion = read_csv("insertion.csv")
merge = read_csv("merge.csv")
quick = read_csv("quick.csv")
selection = read_csv("selection.csv")
radix = read_csv("radix.csv")
shell = read_csv("shell.csv")

x_axis = list(bubble.columns)
x_axis.remove('r\\c')

cases = ['Arranjos com elementos em ordem não decrescente',
         'Arranjos com elementos em ordem não crescente.',
         'Arranjos com elementos 100% aleatórios.',
         'Arranjos com 75% de seus elementos ordenados.',
         'Arranjos com 50% de seus elementos ordenados.',
         'Arranjos com 25% de seus elementos ordenados.']

for i in range(6):

    fig = go.Figure(data=[
        go.Scatter(name='Bubble Sort', x=x_axis, y=bubble.iloc[i][1:]),
        go.Scatter(name='Insertion Sort' ,x=x_axis, y=insertion.iloc[i][1:]),
        go.Scatter(name='Merge Sort', x=x_axis, y=merge.iloc[i][1:]),
        go.Scatter(name='Quick Sort' ,x=x_axis, y=quick.iloc[i][1:]),
        go.Scatter(name='Selection Sort', x=x_axis, y=selection.iloc[i][1:]),
        go.Scatter(name='Radix Sort' ,x=x_axis, y=radix.iloc[i][1:]),
        go.Scatter(name='Shell Sort' ,x=x_axis, y=shell.iloc[i][1:], line=dict(color='rgb(0, 0, 0)'))
    ])
    fig.update_layout(
        title={'text': cases[i]},
        xaxis_title="Tamanho dos Arranjos",
        yaxis_title="Tempo de execução (milissegundos)",
        font=dict(
            size=18,
            color="#7f7f7f"
        )
    )
    fig.update_yaxes(type='log')
    fig.show()



"""
df = read_csv("result.csv")

x_axis = df.len

y_ls = df.ls

y_bs = df.bs

fig = go.Figure(data=[
go.Scatter(name='Linear Search', x=x_axis, y=y_ls),
go.Scatter(name='Binary Search' ,x=x_axis, y=y_bs)
])

fig.update_yaxes(type='log')

fig.update_layout(
    title={
        'text': "Linear Search x Binary Search",
        'y':0.9,
        'x':0.5,
        'xanchor': 'center',
        'yanchor': 'top'},
    xaxis_title="Array Length",
    yaxis_title="Time (milliseconds)",
    font=dict(
        size=18,
        color="#7f7f7f"
    )
)

fig.show()

df = read_csv("recursive.csv")

x_axis = df.len

y_rbs = df.rbs

y_bs = df.bs

fig = go.Figure(data=[
go.Scatter(name='Recursive', x=x_axis, y=y_rbs),
go.Scatter(name='Iterative' ,x=x_axis, y=y_bs)
])

fig.update_yaxes(type='log')

fig.update_layout(
    title={
        'text': "Recursive x Iterative",
        'y':0.9,
        'x':0.5,
        'xanchor': 'center',
        'yanchor': 'top'},
    xaxis_title="Array Length",
    yaxis_title="Time (miliseconds)",
    font=dict(
        size=18,
        color="#7f7f7f"
    )
)

fig.show()
"""


