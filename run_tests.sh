#!/bin/bash

mkdir -p results

echo "The experiment will start, this can take some time!"

./build/test_shell_sort > ./results/shell.csv &
./build/test_bubble_sort > ./results/bubble.csv &
./build/test_insertion_sort > ./results/insertion.csv &
./build/test_quick_sort > ./results/quick.csv &
./build/test_selection_sort > ./results/selection.csv &
./build/test_merge_sort > ./results/merge.csv &
./build/test_radix_sort > ./results/radix.csv &

wait
echo "All done!!!"