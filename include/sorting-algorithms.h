#ifndef SORTING_ALGORITHMS_H
#define SORTING_ALGORITHMS_H

#include <vector>
#include <stack>
#include <iterator>

namespace sa {

    /// just an alias for an integer type.
    using value_type = unsigned int;

    /// Swap (Auxiliary method);
    void swap (value_type * a, value_type * b);

    /// Insertion Sort.
    void insertion_sort(value_type * first, value_type * last);

    /// Selection Sort.
    void selection_sort(value_type * first, value_type * last);

    /// Bubble Sort.
    void bubble_sort(value_type * first, value_type * last);
    
    /// Shell Sort.
    void shell_sort(value_type * first, value_type * last);

    /// Quick Sort.
    void quick_sort(value_type * first, value_type * last);

    /// Partiton (Quick sort auxiliary function)
    size_t partition (value_type * first, value_type * last);

    /// Merge Sort.
    void merge_sort(value_type * first, value_type * last);

    /// Merge arrays (merge 2 arrays, Merge sort auxiliary function)
    void merge_arrays(value_type * first, size_t left, size_t mid, size_t right);

    /// get lower value between 2 (Merge sort auxiliary function)
    size_t lower(size_t a, size_t b);

    /// Radix Sort.
    void radix_sort(value_type * first, value_type * last);

    /// Get higher (Radix sort auxiliary function)
    value_type higher(value_type * first, value_type * last);
    
    /// Count (Radix sort auxiliary function)
    void count_x(value_type * first, value_type * last, value_type x);
}


#endif