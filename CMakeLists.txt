CMAKE_MINIMUM_REQUIRED(VERSION 3.6)
PROJECT(sorting_algorithms)

SET(CMAKE_CXX_STANDARD 11)

SET(SOURCES
    src/sorting-algorithms.cpp
    src/test_bubble_sort.cpp
)

ADD_EXECUTABLE(test_bubble_sort ${SOURCES})

SET(SOURCES
    src/sorting-algorithms.cpp
    src/test_insertion_sort.cpp
)

ADD_EXECUTABLE(test_insertion_sort ${SOURCES})

SET(SOURCES
    src/sorting-algorithms.cpp
    src/test_selection_sort.cpp
)

ADD_EXECUTABLE(test_selection_sort ${SOURCES})


SET(SOURCES
    src/sorting-algorithms.cpp
    src/test_shell_sort.cpp
)

ADD_EXECUTABLE(test_shell_sort ${SOURCES})

SET(SOURCES
    src/sorting-algorithms.cpp
    src/test_quick_sort.cpp
)

ADD_EXECUTABLE(test_quick_sort ${SOURCES})

SET(SOURCES
    src/sorting-algorithms.cpp
    src/test_merge_sort.cpp
)

ADD_EXECUTABLE(test_merge_sort ${SOURCES})

SET(SOURCES
    src/sorting-algorithms.cpp
    src/test_radix_sort.cpp
)

ADD_EXECUTABLE(test_radix_sort ${SOURCES})



