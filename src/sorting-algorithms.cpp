#include "../include/sorting-algorithms.h"

namespace sa {

    // Swap (Auxiliary method)
    void swap (value_type * a, value_type * b){
    /**
    * Swaps two elements from their address.
    * @param a: The first value;
    * @param b: The second value.
    */
        value_type tmp{*b};
        *b = *a;
        *a = tmp;
    }

    /// Insertion Sort
    void insertion_sort( value_type * first, value_type * last){
    /**
    * Sort a target array using insertion sort algorithm.
    * @param first: begining of the target array;
    * @param last: ending of the target array.
    */
        size_t length{size_t(last - first)};
        int aux;
        value_type target;
        for (size_t i{1}; i<length;++i){
            aux = i - 1;
            target = *(first + i);
            while (aux >= 0 and *(first + aux) > target){
                *(first + aux + 1) = *(first + aux);
                aux -= 1;
            }
            *(first + aux + 1) = target;
        }

    }

    /// Selection Sort
    void selection_sort( value_type * first, value_type * last){
    /**
    * Sort a target array using selection sort algorithm.
    * @param first: begining of the target array;
    * @param last: ending of the target array.
    */
        size_t length{size_t(last - first)};
        size_t target;
        for (size_t i{0}; i<length-1;++i){
            target = i;
            for (size_t aux{i+1}; aux<length; ++aux){
                if (*(first+aux) < *(first+target)){
                    target = aux;
                }
            }
            swap(first+target, first+i);
        }
    }

    /// Bubble Sort
    void bubble_sort( value_type * first, value_type * last){
    /**
    * Sort a target array using bubble sort algorithm.
    * @param first: begining of the target array;
    * @param last: ending of the target array.
    */
        size_t length{size_t(last - first)};
        for (size_t i{0}; i<length; ++i){
            for (size_t k{0}; k<length-i-1; ++k){
                if(*(first+k)>*(first+k+1))
                    swap(first+k, first+k+1);
            }
        }
    }
    
    /// Shell Sort
    void shell_sort( value_type * first, value_type * last){
    /**
    * Sort a target array using shell sort algorithm.
    * @param first: begining of the target array;
    * @param last: ending of the target array.
    */
        size_t length{size_t(last - first)}, k;
        value_type tmp;
        for (size_t gap{length/2}; gap > 0; gap /=2){
            for (size_t i{gap}; i<length; ++i){
                tmp = *(first+i);
                for (k = i ; k >= gap and *(first+k-gap) > tmp; k -= gap){
                    *(first+k) = *(first+k-gap);
                }
                *(first+k) = tmp;
            }
        }
    }

    /// partition: quick sort auxiliary  function
    size_t partition (value_type * first, value_type * last){
    /** partition: Quick sort auxiliary  function.
    * Given an array, creates two new paritions separating values greater than pivot form values less than pivot.
    * Pivot is always the last element. 
    * @param first: begining of the target array;
    * @param last: ending of the target array;
    * @return: the pivots's new position.
    */
        value_type * pivot{last};
        value_type * aux {first};
        size_t idx = 0;
        while (aux<last){
            if (*aux<*pivot){
                swap(aux, first+idx);
                ++idx;
            }
            ++aux;
        }
        swap(first+idx, last);
        return idx;
    }

    /// Quick Sort
    void quick_sort( value_type * first, value_type * last){
    /**
    * Sort a target array using quick sort algorithm.
    * @param first: begining of the target array;
    * @param last: ending of the target array.
    */
        std::stack<std::pair<value_type*, value_type*>> stk;
        value_type * start{first}, * end{last-1}, * pivot;
        stk.push(std::make_pair(start, end));
        
        while (!stk.empty()){
            start = stk.top().first;
            end = stk.top().second;
            stk.pop();

            pivot = start + partition(start, end);

            if (pivot-1 > start){
                stk.push(std::make_pair(start, pivot-1));
            }

            if (pivot+1 < end){
                stk.push(std::make_pair(pivot+1, end));
            }

        }
    }

    /// Merge arrays (merge 2 arrays, Merge sort auxiliary method)
    void merge_arrays(value_type * first, size_t left, size_t mid, size_t right){
    /** merge_arrays: Merge sort auxiliary  function.
    * Given 2 non-sorted sub-arrays, merge them into one sorted array.
    * @param first: begining of the first array;
    * @param mid: ending of the first array, begining of the second; 
    * @param last: ending of the second array.
    */
        size_t lenL{mid-left+1}, lenR{right-mid};
        value_type left_array[lenL];
        value_type right_array[lenR];
        size_t il, ir;
        for (il = 0; il < lenL; ++il)
            left_array[il] = *(first+left+il);
        for (ir = 0; ir < lenR; ++ir)
            right_array[ir] = *(first+mid+ir+1);
        
        il = 0;
        ir = 0;

        while (il < lenL and ir < lenR){
            if (left_array[il]<=right_array[ir]){
                *(first+(left++)) = left_array[il++];
            }
            else{
                *(first+(left++)) = right_array[ir++];
            }

        }

        while (il < lenL){
            *(first+(left++)) = left_array[il++];
        }
        while (ir < lenR){
            *(first+(left++)) = right_array[ir++];
        }
    }

    /// get lower value between 2 (Merge sort auxiliary function)
    size_t lower(size_t a, size_t b){
    /** lower: Merge sort auxiliary  function.
    * Given 2 size values, returns the lower.
    * @param a: first size value;
    * @param b: second size value;
    * @return: the lower value. 
    */
        return (a<b)? a:b;
    }

    /// Merge Sort
    void merge_sort( value_type * first, value_type * last){
    /**
    * Sort a target array using merge sort algorithm.
    * @param first: begining of the target array;
    * @param last: ending of the target array.
    */
        size_t length{size_t(last - first)}, mid, right;
        for (size_t i{1}; i<=length-1; i*=2){

            for (size_t left{0}; left<length-1; left+=i*2){

                mid = lower(left+i-1, length-1);
                right = lower(left + 2*i - 1, length-1);

                merge_arrays(first, left, mid, right);
            }
        }
    }

    /// Get higher (Radix sort auxiliary function)
    value_type higher(value_type * first, value_type * last){
    /** higher: Radix sort auxiliary  function.
    * Given a target array, returns ther higher element.
    * @param first: begining of the target array;
    * @param last: ending of the target array;
    * @return: the higher value.
    */
        size_t length{size_t(last - first)};
        value_type h{*first};
        for (size_t k{1}; k<length;++k){
            if (h < *(first+k))
                h = *(first+k);
        }
        return h;
    }
    
    /// Count (Radix sort auxiliary function)
    void count_x(value_type * first, value_type * last, value_type x){
    /** count_x: Radix sort auxiliary  function.
    * Group an array's elemets acording the iteration digit.
    * @param first: begining of the target array;
    * @param last: ending of the target array;
    * @param x: iteration digit.
    */
        size_t length{size_t(last - first)};
        value_type result[length];
        int count[10]{0};
        
        for (size_t k{0}; k<length;++k)
            ++count[(*(first+k)/x) % 10];
      
        for (size_t k{1}; k<10;++k)
            count[k] += count[k-1];

        for (int i{(int)length-1}; i >= 0;--i){
            result[count[(*(first+i)/x) % 10] - 1] = *(first+i);
            --count[(*(first+i)/x) % 10];
        }
        
        for (size_t k{0}; k<length;++k)
            *(first+k) = result[k];

    }

    /// Radix Sort
    void radix_sort(value_type * first, value_type * last){
    /**
    * Sort a target array using radix sort algorithm.
    * @param first: begining of the target array;
    * @param last: ending of the target array.
    */
        size_t length{size_t(last - first)};
        value_type high = higher(first, last);
        for (value_type x{1}; high / x > 0; x*=10)
            count_x(first, last, x);
    }

}