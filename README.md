# sorting_algorithms
Empirical analysis of some sorting algorithms.

# Requirements:
- bash;
- g++;
- make; 
- cmake.

Or you can run with docker, you can install it following these steps [get docker](https://docs.docker.com/get-docker/).

# Compile:

- Normally:
    ```sh
    cd sorting_algorithms
    cmake -S . -Bbuild
    cd build
    make
    ```
- Docker:
    ```sh
    docker build -t sa .
    ```

# Run:

Each algorithm has a binary to test it self:

```sh 
./build/test_bubble_sort
./build/test_insertion_sort
./build/test_merge_sort
./build/test_quick_sort
./build/test_radix_sort
./build/test_selection_sort
./build/test_shell_sort
```

- Normally:
    ```sh
    ./build/test_<algorithm>_sort
    ```
- Docker:
    ```sh
    docker run -it --rm sa ./build/test_<algorithm>_sort
    ```
    
## Run entire experiment:

- Normally:

    ```sh
    bash run_tests.sh
    ```
- Docker:

    ```sh
    docker run -it --rm -v $PWD/results:/sorting_algorithms/results sa
    ```
